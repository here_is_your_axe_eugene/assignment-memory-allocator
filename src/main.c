//
// Created by Maxim on 31.12.2021.
//

#include <stdio.h>
#include "mem_internals.h"
#include "mem.h"

void *heap_init_test(size_t initial_size) {
    printf("Initiating heap with size %zu.\n", initial_size);
    void * first_header_address = heap_init(initial_size);
    printf("Heap was initiated.\n");
    return first_header_address;
}

void *malloc_test(size_t size) {
    printf("Allocating %zu bytes.\n", size);
    void *address = _malloc(size);
    printf("%zu bytes have been allocated.\n", size);
    return address;
}

void free_test(void *p) {
    printf("Trying to free p on %p\n", p);
    _free(p);
    printf("Address on %p has been released.\n", p);
}

void debug_heap_test( FILE* f,  void const* ptr ) {
    debug_heap(f, ptr);
    printf("\n");
}

int main(void) {
    size_t heap_init_size = 8192;
    size_t bytes_1 = 1024, bytes_2 = 10000, bytes_3 = 16385;
    void *p1, *p2, *p3;

    struct block_header *init_header = heap_init_test(heap_init_size);

    debug_heap_test(stdout, init_header);

    p1 = malloc_test(bytes_1);
    debug_heap_test(stdout, init_header);
    p2 = malloc_test(bytes_2);
    debug_heap_test(stdout, init_header);
    p3 = malloc_test(bytes_3);
    debug_heap_test(stdout, init_header);

    free_test(p2);
    debug_heap_test(stdout, init_header);
    free_test(p1);
    debug_heap_test(stdout, init_header);

    p1 = malloc_test(bytes_1);
    debug_heap_test(stdout, init_header);
    p2 = malloc_test(bytes_1);
    debug_heap_test(stdout, init_header);


    free_test(p3);
    debug_heap_test(stdout, init_header);

    free_test(p2);
    debug_heap_test(stdout, init_header);
    free_test(p1);
    debug_heap_test(stdout, init_header);

    p1 = malloc_test(0);
    debug_heap_test(stdout, init_header);
    if (p1 == NULL) {
        printf("Everything is ok.\n");
    } else {
        printf("Bruh.\n");
    }

    p1 = malloc_test(1);
    debug_heap_test(stdout, init_header);

    return 0;
}
